/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.pojo;

import java.io.Serializable;

/**
 *
 * @author buls
 */
public class PlatformCallback implements Serializable {
    private String callBackUrl;

    public PlatformCallback() {
        
    }
    
    public PlatformCallback(String callBack) {
        this.callBackUrl = callBackUrl;
    }
    
    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }
    
    
}
