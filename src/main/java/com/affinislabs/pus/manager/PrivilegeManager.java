/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import com.affinislabs.pus.data.manager.PrivilegeDataManagerLocal;
import com.affinislabs.pus.model.Privilege;

/**
 *
 * @author buls
 */
@Stateless
public class PrivilegeManager implements PrivilegeManagerLocal{
        
    @EJB
    private PrivilegeDataManagerLocal privilegeDataManager;
    
    @Override
    public Privilege getPrivilege(String privilegeId) {
        return privilegeDataManager.get(privilegeId);
    }
    
    
    @Override
    public List<Privilege> getAllPrivileges() {
        List<Privilege> appPrivileges = new ArrayList<>();
        List<Privilege> privileges = privilegeDataManager.getAllPrivileges();
        for (Privilege privilege : privileges) {
            appPrivileges.add(getAppPrivilege(privilege));
        }
        return appPrivileges;
    }
    
   
    @Override
    public Privilege getAppPrivilege(Privilege privilege) {
        Privilege appPrivilege = new Privilege();
        
        appPrivilege.setPrivilegeId(privilege.getPrivilegeId());
        appPrivilege.setDescription(privilege.getDescription());
                
        return appPrivilege;
    }
    
    @Override
    public Privilege addPrivilege(String privilegeId, String description, String menuLink) {        
        Privilege appPrivilege = null; 
        
        if (privilegeId != null && description != null && menuLink != null) {
            Privilege privilege = new Privilege();
            
            privilege.setPrivilegeId(privilegeId);
            privilege.setDescription(description);
                        
            privilegeDataManager.create(privilege);
            appPrivilege = getAppPrivilege(privilege);
            return appPrivilege;
        } 
        return appPrivilege;
    }
   
    @Override
    public Boolean deletePrivilege(String privilegeId) {
        if (privilegeId != null){
            Privilege privilege = privilegeDataManager.get(privilegeId);
                       
            privilegeDataManager.delete(privilege);
            return true;
        }
        return false;    
    }
    
    @Override
    public Privilege updatePrivilege(String privilegeId, String description, String menuLink, String menuIcon) {        
        Privilege appPrivilege = null;         
        
        if (privilegeId != null && description != null && menuLink != null) {
            Privilege privilege = privilegeDataManager.get(privilegeId);
            
            privilege.setDescription(description);            
            
            privilegeDataManager.update(privilege);
            appPrivilege = getAppPrivilege(privilege);
            return appPrivilege;
        } 
        return appPrivilege;
    }
}
