/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.service;

import com.affinislabs.pus.manager.UserManagerLocal;
import com.affinislabs.pus.pojo.AppUser;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.affinislabs.pus.pojo.PlatformCallback;
import com.affinislabs.pus.util.exception.DuplicateUserKeyException;
import com.affinislabs.pus.util.exception.InternalException;
import com.affinislabs.pus.util.exception.InvalidEmailFormatException;
import com.affinislabs.pus.util.exception.InvalidLoginCredentialsException;
import com.affinislabs.pus.util.exception.InvalidUserTypeException;
import com.affinislabs.pus.util.exception.NullUserAttributeException;
import com.affinislabs.pus.util.exception.UserDoesNotExistException;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/user")
public class UserService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    UserManagerLocal userManager;
       
    // http://localhost:8080/us/api/v1/user/?email=lateefah.abdulkareem@intellectualapps.com&first-name=Lateefah&last-name=Abdulkareem&phone-number=08073384437&password=password&user-type=sh
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@QueryParam("email") String email,
            @QueryParam("password") String password) throws NullUserAttributeException, 
            InvalidEmailFormatException, InvalidUserTypeException, DuplicateUserKeyException {  
        
        AppUser appUser = userManager.addUser(email, password, "us");
        return Response.ok(appUser).build();
           
    }
    
    @Path("/email")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyUserEmail(@QueryParam("email") String encodedEmail) 
            throws NullUserAttributeException, UserDoesNotExistException,
            InternalException {  
        
        PlatformCallback callBack = userManager.verifyEmail(encodedEmail);
        System.out.println(callBack.getCallBackUrl());
        return Response.ok(callBack).build();
           
    }
    
    @Path("/{email}/phone/code/{sms-code}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyUserSmsCode(@PathParam("email") String email, 
            @PathParam("sms-code") String smsCode) 
            throws NullUserAttributeException, 
            InvalidUserTypeException, UserDoesNotExistException {  
        
        AppUser appUser = userManager.verifySmsCode(email, smsCode);
        return Response.ok(appUser).build();
           
    }

    @Path("/authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateUser(@QueryParam("email") String email, 
            @QueryParam("password") String password) 
            throws NullUserAttributeException, InvalidEmailFormatException, InvalidLoginCredentialsException {  
        
        AppUser appUser = userManager.authenticateUser(email, password);
        return Response.ok(appUser).build(); 
    }
    
    @Path("/{email}/phone/code")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response resendUserSmsVerificationCode(@PathParam("email") String email) 
            throws NullUserAttributeException, 
            InvalidUserTypeException, UserDoesNotExistException {  
        
        AppUser appUser = userManager.resendSmsVerification(email);

        return Response.ok(appUser).build();
           
    }
    
    @Path("/{email}/email/link")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response resendUserEmailVerificationLink(@PathParam("email") String email) 
            throws NullUserAttributeException, 
            InvalidUserTypeException, UserDoesNotExistException {  
        
        AppUser appUser = userManager.resendEmailVerification(email);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/password")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetUserPassword(@QueryParam("email") String email) 
            throws NullUserAttributeException, InvalidEmailFormatException, UserDoesNotExistException {  
        
        AppUser appUser = userManager.resetPassword(email) ;
        return Response.ok(appUser).build();
           
    }
    
    @Path("/password")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response setNewPassword(@QueryParam("email") String email, 
            @QueryParam("password") String password) 
            throws NullUserAttributeException, InvalidEmailFormatException {  
        
        AppUser appUser = userManager.setNewPassword(email, password);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{email}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDetails(@PathParam("email") String email) 
            throws NullUserAttributeException, InvalidEmailFormatException, UserDoesNotExistException {  
        
        AppUser appUser = userManager.getUserDetails(email) ;
        return Response.ok(appUser).build();
           
    }
    
    @Path("/password-token")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticatePasswordResetToken(@QueryParam("email") String email,
            @QueryParam("password-reset-token") String token) 
            throws NullUserAttributeException, UserDoesNotExistException {  
        
        String authentic = userManager.authenticatePasswordResetToken(email, token) ;
        return Response.ok(authentic).build();
           
    }
    
    //to allow developers remove their test email
    @Path("/removedon")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeUser() 
            throws NullUserAttributeException, 
            InvalidUserTypeException, UserDoesNotExistException {  
        
        userManager.removeDonCasino();
        return Response.ok("removed").build();
           
    }
    
    @Path("/removesadiq")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeSadiq() 
            throws NullUserAttributeException, 
            InvalidUserTypeException, UserDoesNotExistException {  
        
        userManager.removeSadiq();
        return Response.ok("removed").build();
           
    }
    
    @Path("/removelattie")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeLattie() 
            throws NullUserAttributeException, 
            InvalidUserTypeException, UserDoesNotExistException {  
        
        userManager.removeLattie();
        return Response.ok("removed").build();
           
    }
}
