 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.affinislabs.pus.data.manager;


 import javax.ejb.Local;
 import java.util.List;
import com.affinislabs.pus.model.Role;
import com.affinislabs.pus.model.RolePrivilege;

/**
 *
 * @author buls
 */
@Local
public interface RoleDataManagerLocal {
    
    Role create(Role role);

    Role update(Role role);

    Role get(String roleId);

    void delete(Role role);

    List<Role> getAllRoles();
    
    List<RolePrivilege> getByRole(String roleId);
    
    RolePrivilege addRolePrivilege(RolePrivilege rolePrivilege);

    List<RolePrivilege> getRoleAndPrivilege(String roleId, String privilegeId);
    
    void deleteRolePrivilege(RolePrivilege rolePrivilege);
}
