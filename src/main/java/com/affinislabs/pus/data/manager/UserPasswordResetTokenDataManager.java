/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.data.manager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.affinislabs.pus.data.provider.DataProviderLocal;
import com.affinislabs.pus.model.UserPasswordResetToken;

/**
 *
 * @author buls
 */
@Stateless
public class UserPasswordResetTokenDataManager implements UserPasswordResetTokenDataManagerLocal {    

    @EJB
    private DataProviderLocal crud;    
    
    @Override
    public UserPasswordResetToken create(UserPasswordResetToken token) {
        return crud.create(token);
    }

    @Override
    public UserPasswordResetToken get(String email) {
        return crud.find(email, UserPasswordResetToken.class);
    }

    @Override
    public void delete(UserPasswordResetToken token) {
        crud.delete(token);
    }
   
}
