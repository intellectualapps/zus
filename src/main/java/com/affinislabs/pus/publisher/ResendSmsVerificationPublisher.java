/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import com.affinislabs.pus.model.User;

/**
 * This class publishes new user created events to the new user topic.
 * Topic connection and Topic need to be configured in Glassfish for this work
 * @author buls
 */

@Singleton
public class ResendSmsVerificationPublisher {

    @Resource
    TimerService timerService;
    private User user = null;

    private Logger logger = Logger.getLogger(
            ResendSmsVerificationPublisher.class.getName());

    public void publishResendSmsVerificationEvent(User user, long timeout) {
        logger.info(ResendSmsVerificationPublisher.class.getName() + 
                "Publishing resend sms verification event for: " + user.getEmail());

        this.user = user;
        Timer timer = timerService.createSingleActionTimer(timeout,
                new TimerConfig());
    }

    @Timeout
    private void prepareBroadcastEvent(Timer timer) {
        try {
            String message = new ObjectMapper().writeValueAsString(user);
            broadcastEvent(message);
        } catch (Exception e) {
            logger.severe(ResendSmsVerificationPublisher.class.getName() + 
                    " Failed to prepare resend sms verfication broadcast: " + e.getMessage());
        }
    }

    private void broadcastEvent(String message) {
        logger.info(ResendSmsVerificationPublisher.class.getName() + 
                " Broadcasting resend sms verification event: " + message);

        TopicConnection topicConnection = null;
        try {

            Context initialContext = new InitialContext();
            TopicConnectionFactory qcf = (TopicConnectionFactory) initialContext.lookup("jms/cccon");
            topicConnection = qcf.createTopicConnection();
            TopicSession topicSession = topicConnection.createTopicSession(false, 
                    Session.AUTO_ACKNOWLEDGE);

            TextMessage textMessage = topicSession.createTextMessage();
            textMessage.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
            textMessage.setText(message);

            Topic newUserCreatedTopic = (Topic) initialContext.lookup("jms/resendSmsVerificationTopic");
            MessageProducer sender = topicSession.createProducer(newUserCreatedTopic);

            sender.send(textMessage);

            logger.info(ResendSmsVerificationPublisher.class.getName() + 
                    " resend sms verification event published with data: " + message);
        } catch (Exception e) {
            logger.severe(ResendSmsVerificationPublisher.class.getName() + 
                    " Error occured trying to broadcast resend sms verification event: " + e.getMessage());
        } finally {
            if (topicConnection != null) {
                try {
                    topicConnection.close();
                } catch (Exception e) {
                    logger.severe(ResendSmsVerificationPublisher.class.getName() + 
                            " Failed to close topic connection: " + e.getMessage());
                }
            }
        }
    }

}
