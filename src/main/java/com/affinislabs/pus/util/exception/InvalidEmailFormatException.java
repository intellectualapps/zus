/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util.exception;

/**
 *
 * @author Lateefah
 */ 
public class InvalidEmailFormatException extends AppException {
    
    private static final long serialVersionUID = 1L;
    
    public InvalidEmailFormatException(int status, int code, String message,
			String developerMessage, String link) {
        super(status, code, message, developerMessage, link);		
    } 
    
}
