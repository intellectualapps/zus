/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.*;
import java.util.Date;    
import javax.crypto.KeyGenerator;
import javax.ejb.Stateless;
import com.affinislabs.pus.pojo.AppUser;

/**
 *
 * @author Lateefah
 */
@Stateless
public class JWT {    
    SecretKey secretKey = new SecretKey();
    private String issuer = "us";
            
    public String createJWT(AppUser user, long ttlMillis) {        
        
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey.getSecret());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        
        JwtBuilder builder = Jwts.builder()
                                    //.setId(id)
                                    .setIssuedAt(now)
                                    .setSubject(user.getEmail())
                                    .claim("roles", user.getUserRolesAsJson())
                                    .setIssuer("us")
                                    .signWith(signatureAlgorithm, signingKey);

        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        return builder.compact();
    }
    
    public void parseJWT(String jwt) {
 
        Claims claims = Jwts.parser()         
           .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getSecret()))
           .parseClaimsJws(jwt).getBody();
        System.out.println("ID: " + claims.getId());
        System.out.println("Subject: " + claims.getSubject());
        System.out.println("Issuer: " + claims.getIssuer());
        System.out.println("Expiration: " + claims.getExpiration());
    }
    
}
