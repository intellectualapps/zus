SET foreign_key_checks = 0;
DROP TABLE `privilege`;
DROP TABLE `role`;
DROP TABLE `role_privilege`;
DROP TABLE `user_role`;
SET foreign_key_checks = 1;

CREATE TABLE `privilege` (
  `id` varchar(32) NOT NULL,
  `privilege` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role` (
  `id` varchar(32) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_privilege` (
  `role_id` varchar(32) NOT NULL,
  `privilege_id` varchar(32) NOT NULL,
  PRIMARY KEY (`role_id`,`privilege_id`),
  KEY `privilege_id` (`privilege_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_role` (
  `email` varchar(50) NOT NULL,
  `role_id` varchar(32) NOT NULL,
  PRIMARY KEY (`email`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`email`) REFERENCES `user` (`email`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `privilege` (`id`, `privilege`) VALUES ('ADD_DRIVER', 'Add a driver to profile');
INSERT INTO `privilege` (`id`, `privilege`) VALUES ('ADD_VEHICLE', 'Add vehicle to profile');
INSERT INTO `privilege` (`id`, `privilege`) VALUES ('VIEW_DRIVERS', 'View list of registered drivers');
INSERT INTO `privilege` (`id`, `privilege`) VALUES ('VIEW_VEHICLE', 'View list of registered vehicles');

INSERT INTO `role` (`id`, `role`) VALUES ('STAKEHOLDER', 'A stakeholder on the platform');
INSERT INTO `role` (`id`, `role`) VALUES ('DRIVER', 'A driver on the platform');
INSERT INTO `role` (`id`, `role`) VALUES ('RIDER', 'A rider on the platform');

INSERT INTO `role_privilege` (`role_id`, `privilege_id`) VALUES ('STAKEHOLDER', 'ADD_DRIVER');
INSERT INTO `role_privilege` (`role_id`, `privilege_id`) VALUES ('STAKEHOLDER', 'ADD_VEHICLE');
INSERT INTO `role_privilege` (`role_id`, `privilege_id`) VALUES ('STAKEHOLDER', 'VIEW_DRIVER');
INSERT INTO `role_privilege` (`role_id`, `privilege_id`) VALUES ('STAKEHOLDER', 'VIEW_VEHICLE');



