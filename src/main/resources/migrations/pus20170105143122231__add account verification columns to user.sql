ALTER TABLE `user` 
ADD COLUMN `sms_verification_code` VARCHAR(7) NULL DEFAULT 0 COMMENT '' AFTER `creation_date`,
ADD COLUMN `email_address_verified` BIT(1) NULL DEFAULT 0 COMMENT '' AFTER `sms_verification_code`;

